---
title: "Inferencia de redes dinámicas: El Doctorado de Alguien"
author: "Gente"
output:
  html_document:
    df_print: paged
    toc: yes
    toc_float:
      collapsed: false
    toc_depth: 4
    number_sections: false
    smooth_scroll: false
    code_folding: hide
    code_download: true
  pdf_document:
    latex_engine: xelatex
    toc: true
    toc_depth: 4
    number_sections: true
editor_options:
  chunk_output_type: inline
date: "`r format(Sys.time(), '%d %B, %Y')`"
urlcolor: blue
knit: (function(inputFile, encoding) {
  rmarkdown::render(inputFile, encoding = encoding, output_dir = "output/renders") })
---

```{r setup, message=F}
knitr::opts_chunk$set(message = F)
knitr::opts_knit$set(root.dir = here::here())

library(tidyverse)
devtools::load_all()  # Cargar este proyecto como paquete.

plot3d_goldbeter <- function(sim_results_df) with(data = sim_results_df, {
  # https://r-graph-gallery.com/3d_scatter_plot.html
  # https://stackoverflow.com/a/63765069
  # https://www.rdocumentation.org/packages/rgl/versions/1.1.3/topics/rglwidget
  
  # Save previous settings, and set NULL device for RGL.
  save <- options(rgl.useNULL=TRUE)
  
  rgl::plot3d( 
    x=C, 
    y=M, 
    z=X, 
    type="n",   # Plot nothing, but set up the axes # type = 'l', # type = 's',
    # radius = .1,
    xlab="C", 
    ylab="M",
    zlab="X")
  
  for (d in split(sim_results_df, ~iter)) with(d, {
    rgl::lines3d(
      x=C, 
      y=M, 
      z=X, 
      col = iter)
  })

  # To display in an R Markdown document:
  widget <- rgl::rglwidget()
  print(widget)
  
  # Restore previous settings
  options(save)
})

reduce_cor_mat <- function(m){
  m.names <- outer(colnames(m), rownames(m), FUN = paste0)
  c.names <- setNames(m[lower.tri(m)], nm = m.names[lower.tri(m)])
  return(c.names)
}
```

## About

Exploratory analysis of simulated data for system inference / charachterization.

## Load data

Cargo datos de 100 simulaciones, con condiciones iniciales uniformes, 
con o sin ruido uniforme agregado, a tiempos largos o cortitos.

```{r}
results.df.full <- read.csv(file=gzfile("data/results.full.df.csv.gz"))

results.df <- 
  read.csv(file=gzfile("data/results.df.csv.gz"))
results.df.acq_noise <- 
  read.csv(file=gzfile("data/results.df.acq_noise.csv.gz"))

results.df.noise025 <- 
  read.csv(file=gzfile("data/results.df.noise025.csv.gz"))
results.df.noise025.acq_noise <-
  read.csv(file=gzfile("data/results.df.noise025.acq_noise.csv.gz"))
```

## Processing and analysis

Choose a dataset:

```{r}
dataset <- results.df
# dataset <- results.df.noise025
```

### Correlación lineal

Ver: https://stat.ethz.ch/R-manual/R-devel/library/stats/html/cor.html

Average pearson corfficients:

```{r}
r.pearson <- dataset %>% 
  split(~iter) %>% 
  sapply(function(d){
    m <- d %>% select(C, M, X) %>% cor(method="pearson") # var() cov()
    m.names <- outer(colnames(m), rownames(m), FUN = paste0)
    setNames(m[lower.tri(m)], nm = m.names[lower.tri(m)])
  }) %>% 
  t()

head(r.pearson)

colMeans(r.pearson)
```

Average Spearman coefficients:

```{r}
r.spearman <- dataset %>% 
  split(~iter) %>% 
  sapply(function(d){
    m <- d %>% select(C, M, X) %>% cor(method="spearman") # var() cov()
    reduce_cor_mat(m)
  }) %>% 
  t()

head(r.spearman)

colMeans(r.spearman)
```

I'm not sure what I expected.

#### Sign of the correlation and the role of time

In the 2D plots of the Goldbeter simulation it can be seen that two distinct situations 
(correlated increase and  decrease across time) lead to the same _positive_ correlation 
coefficient.

The slope across time is not considered (obviously, in retrospect) because 
time was not used in the calculation of the correlation. I was wrong to expect positive
correlations between species to come from simultaneous increases, and negative correlations
from simultaneous decreases.

Negative correlation coefficients indicate anti-correlation (one increases and the other 
decreases), independently of the sign of their slopes against time.

I get the feeling that time should be considered somehow in the interpretation of
correlation coefficients.

There are suggestions online to use lagged correlation (as done for time series).

#### Spearman vs Pearson

Spearman correlations are _maybe_ a _bit_ higher in magnitude. Perhaps because it can 
capture the bit of  non-linearity that shows in the short traces? Wikipedia says that the 
correlation must be "monotonic" to be captured by this method.

#### Bounds for YZ correlation given XY and XZ

Two correlations (C-M nd C-X) are negative, and one (M-X) is positive. This is consistent
with my expectations; if C is anticorrelated with both M and X, then M and X should tend
to correlate positively.

There are several related questions posted to the internet, about what should be the expected
correlation of X and Y, given the correlations between X-Z and Y-Z. For example:

* https://math.stackexchange.com/a/586142
    * _"Say you have X,Y,Z three random variables such that the correlation of X and Y is something and the correlation of Y and Z is something else, what are the possible correlations for X and Z in terms of the other two correlations?"_
* https://stats.stackexchange.com/a/493951/295029
    * _"Correlation range for third random variable given two correlation results"_.
    * Adaptation of this answer: https://math.stackexchange.com/a/586142
* https://stats.stackexchange.com/a/498561/295029
    * Un ejemplo de como Z puede correlacionar con X e Y por separado, pero la correlacion de X con Y ser cero.
    * Otro pero con peces: https://stats.stackexchange.com/a/5753/295029
* https://stats.stackexchange.com/questions/495546/is-it-possible-for-two-random-variables-to-be-negatively-correlated-but-both-be
    * Rain, umbrellas, and getting wet.

There appear to be mathematical bounds for the third correlation, but the conditions are
not clear to me. On the other hand, I get the feeling that it is possible to come up
with "daily situation" examples for any correlation matrix.

I'd like a collection of those, and particularly one for two positive correlations and
a negative third one.

Hay respuestas en SE relevante:

- _Is Correlation Transitive?_: https://stats.stackexchange.com/a/181380/295029
- _How to infer correlations from correlations_: https://stats.stackexchange.com/a/124909/295029

> We may prove that if the correlations are sufficiently close to 1, then X and Z must be
> positively correlated.

#### Distribution of correlations in short traces

Each individual is in a different "place" of the phase diagram. When averaged together,
positive and negative correlation coefficients of different parts of the trace could be
cancelling out. For example,at certain _places_ the correlation between two variables may 
be positive, and at others it may be negative (anti-correlation).
This can be seen in the limit cycle that forms in Goldbeter's system.

Lets have a look at the distributions. Some of the above may become apparent:

```{r}
r.spearman %>% as.data.frame() %>% 
  mutate(id = 1:n()) %>% tidyr::pivot_longer(-id) %>%
  ggplot() + 
  geom_vline(xintercept = c(-0.7, 0.7), color="red") +
  geom_histogram(aes(value), binwidth=0.1) + 
  facet_grid(~name) + theme_minimal()
```

The bimodality in the correlation coefficients shows that sometimes variables correlate,
and sometimes they anti-correlate. This matches what happens when traces form a "loop" 
(or limit cycle in this case) in a phase space diagram (i.e. variables against variables).

_Note_: I briefly considered clustering the data, and checking if the correlations increased
in magnitude (and differentiated in sign). But this would not have added much in principle,
as grouping similar things together will already have them correlate better. This needs 
further elaboration.

Draw the transitivity circle:

```{r}
a <- seq(0, 2*pi + 0.1, by=0.1)
circle <- data.frame(x = sin(a), y = cos(a))

r.spearman %>% as.data.frame() |> 
  # mutate(
  #   MC_XC = MC^2 + XC^2,
  #   MC_XM = MC^2 + XM^2,
  #   XM_XC = XM^2 + XC^2,
  # ) |> 
  # select(-MC, -XC, -XM) |> 
  # mutate(id = 1:n()) |> tidyr::pivot_longer(-id) |> 
  
  ggplot() +
  geom_path(aes(x=x, y=y), data=circle, alpha=.5, linetype=2) +
  geom_point(aes(MC, XC, color="MC_XC")) +
  geom_point(aes(MC, XM, color="MC_XM")) +
  geom_point(aes(XM, XC, color="XM_XC")) +
  theme_minimal()
```


## Full traces

Repeat the above on the full simulations (i.e. using long simulations instead of shorter
segments).

Average Spearman coefficients:

```{r}
r.spearman <- results.df.full %>% 
  split(~iter) %>% 
  sapply(function(d){
    m <- d %>% select(C, M, X) %>% cor(method="spearman") # var() cov()
    m.names <- outer(colnames(m), rownames(m), FUN = paste0)
    setNames(m[lower.tri(m)], nm = m.names[lower.tri(m)])
  }) %>% 
  t()

head(r.spearman)

colMeans(r.spearman)
```
Lets have a look at the distributions.

```{r}
plot_corr_distributions <- r.spearman %>% as.data.frame() %>% 
  mutate(id = 1:n()) %>% tidyr::pivot_longer(-id) %>%
  ggplot() + 
  geom_density(aes(value)) + 
  facet_grid(~name)

plot_corr_distributions
```

On average (and at first sight) MC and XM correlate positively, and XC mostly negatively.

Same thing in a list:

- M correlates positively with C.
- M correlates positively with X.
- _But_ C and X anti-correlate (on most simulations).

I was not expecting this, but it _can_ be seen in the plots nonetheless, overall, sort of...

```{r}
plot3d_goldbeter(results.df.full)
```

Here is a dummy dataset, with positive cor(XY), and a Z variable with negative correlation with X.
This means that cor(YZ) should also be negative, and this is indeed the case:

```{r}
X <- data.frame(
  x=1:10,
  y=1:10,
  z=rev(1:10)
)

m <- cor(X)
m
reduce_cor_mat(m)
```

This shows up in a 3D plot of the dummy data. Each pair of measurements restricts the data
to a plane in 3D space (because they are straight lines). In this way the third YZ pair is
determined by the projection onto the YZ plane of the line formed by the intersection  of 
the XY and XZ planes.

It is confusing in my words, so here are some plots to make it clearer:

```{r}
plot(X[,1:2], type="l", asp = 1)
plot(X[,c(1,3)], type="l", asp = 1)
plot(X[,3:2], type="l", asp = 1)
```

Now in 3D!

```{r}
abc.xy <- pracma::cross(c(1,1,0),c(0,0,1))
abc.xz <- pracma::cross(c(1,0,-1),c(0,1,0))

with(X, {
  # Save previous settings, and set NULL device for RGL.
  # save <- options(rgl.useNULL=TRUE)
  
  rgl::plot3d( 
    x=x, 
    y=y, 
    z=z, 
    type="l",
    xlab="x", 
    ylab="y",
    zlab="z")
  rgl::planes3d(a=abc.xy[1],
                b=abc.xy[2],
                c=abc.xy[3],
                d=0,
                alpha = 0.5, col="green")
  rgl::planes3d(a=abc.xz[1], 
                b=abc.xz[2],
                c=abc.xz[3],
                d=-11,
                alpha = 0.5, col="blue")
  
  # To display in an R Markdown document:
  # widget <- rgl::rglwidget()
  # print(widget)

  # Restore previous settings
  # options(save)
})
```

> Dado que cuando M sube, C sube, y cuando M sube X sube, ¿no hubiera tenido sentido que cuando C suba X también?

A sketch on the same stuff:

```{r}
knitr::include_graphics("notebooks/images/correlation_scenario_for_lines.svg")
```

This is the scenario where the two very high correlations fully determine the third.

However, I'm still missing an explanation for the "two positive and one negative" 
correlation scenario on the full trace dataset (which is far from strong correlations,
as all of them are less than 0.5 in magnitude).

Lets look at a single trace to try catch the pattern:

```{r}
results.df.full %>% filter(iter==1) %>% #select(tiempo, C, M, X) %>% pivot_longer(-tiempo) %>% 
  ggplot() +
  geom_path(aes(C, X, color=tiempo))

results.df.full %>% filter(iter==1) %>% #select(tiempo, C, M, X) %>% pivot_longer(-tiempo) %>% 
  ggplot() +
  geom_path(aes(C, M, color=tiempo))

results.df.full %>% filter(iter==1) %>% #select(tiempo, C, M, X) %>% pivot_longer(-tiempo) %>% 
  ggplot() +
  geom_path(aes(X, M, color=tiempo))
```

- M and X are mostly positively correlated: both increase (or decrease) simultaneously.
  The parts where this reverses is rather "small" in phase space.
- C and X look negatively correlated, but also have some places with positive correlation.
- C and M are mostly positively correlated.

```{r}
results.df.full %>% filter(iter==1) %>% plot3d_goldbeter()
```

### Contradictory correlations

I have no answer for this at the moment.

On the one hand, there is a discrepancy between correlation of the traces as a whole,
and segments of those traces. ¿Is this simpsons's paradox?

On the other hand, the complete trace shows the "two positive and one negative" 
correlation scenario, and I still cant see how/why this is possible. The answers 
[at stats exchange](https://stats.stackexchange.com/a/181380/295029) say that if
two correlations are "weak" then the third one can have any sign.

Here is an example from the marvellous multiversal internet: https://stats.stackexchange.com/a/164476/295029

It is _possible_ to generate data from a multinormal distribution, matching the
"contradictory" correlation matrix I got from the full dynamics.

It cannot be _too contradictory_ though, because then the covariance matrix will cease to
be [positive-semi-definite](https://en.wikipedia.org/wiki/Definite_matrix), and R will 
refues to simlate the data.

Covariance matrices are, by definition, positive-semi-definite. Meaning that the matrix
generating the error **is not** a valid covariance matrix (it may be a valide _somerhingelse_).

```{r}
# See: https://stats.stackexchange.com/a/164476/295029

mu <- c(4.23, 3.01, 2.91)
stddev <- c(1.23, 0.92, 1.32)

XY <- 0.78
XZ <- 0.30 # 0.23
YZ <- -0.35 # -0.27

corMat <- matrix(c(1, XY, XZ,
                   XY, 1, YZ,
                   XZ, YZ, 1),
                 ncol = 3)
corMat
```

```{r}
covMat <- stddev %*% t(stddev) * corMat
covMat

# C <- covMat[,1]
# t(C) %*% covMat %*% C
# apply(covMat, 2, function(C) t(C) %*% covMat %*% C)
```

R fails here if the covariance matrix becomes non-positive-semi-definite:

```{r}
set.seed(1)
set.seed(42)
dat1 <- MASS::mvrnorm(n = 212, mu = mu, Sigma = covMat, empirical = FALSE)
```

Lets look at the data and try to realize why this is possible at low correlation values:

```{r}
plot(dat1[,1:2], asp=1, xlab="x", ylab="y", main=paste("corr.:", XY), 
     sub="Note: this correlation is quite high.")
plot(dat1[,c(1,3)], asp=1, xlab="x", ylab="z", main=paste("corr.:", XZ),
     sub="Note: this blob appears to correlate barely.")
plot(dat1[,2:3], asp=1, xlab="y", ylab="z", main=paste("corr.:", YZ),
     sub="Note: this looks like it anti-correlates.")
```

Ahora en 3D, a ver si ayuda:

- Si giramos el gráfico para ver la proyección XY, se ve la correlación entre XY. Joya.
- Si se rota el espacio de determinada manera, se ve que los datos en realidad son un panqueue.
- Volviendo a la proyección XY, y sarandeando un poquito el gráfico, vemos que el panqueue además de estar inclinado para que haya correlación en XY, está un poquito "levantado" para que haya correlación negagiva entre YZ.
- Volviendo a la proyección XY, y de nuevo moviendo un poquito el grafico, vemos que el plano de los datos está inclinado de forma que aparece una correlación positiva entre entre XZ.

```{r}
rgl::plot3d(dat1, xlab="x", ylab="y", zlab="z")

widget <- rgl::rglwidget()
invisible(print(widget))
```

Con un poco de overplotting se consigue algo interpretable en 2D.

Armo unas funciones para generar y graficar datos a partir de 3 coeficientes de correlación.

```{r}
# set.seed(1)
gen_data <- function(
    mu = c(0.23, 0.01, -0.091), 
    stddev = c(1.23, 0.92, 1.02),
    XY = 0.78, 
    XZ = 0.30,
    YZ = -0.35,
    n_pts=100,
    point_size_scaler=1
  ){
  corMat <- matrix(c(1, XY, XZ,
                     XY, 1, YZ,
                     XZ, YZ, 1),
                   dimnames = list(c("X", "Y", "Z"), c("X", "Y", "Z")),
                   ncol = 3)
  
  covMat <- stddev %*% t(stddev) * corMat
  
  try(
    expr = {
      dat1 <- MASS::mvrnorm(n = n_pts, mu = mu, Sigma = covMat, empirical = FALSE)
      colnames(dat1) <- c("X", "Y", "Z")
      
      plts <- plot_data(dat1, corMat, n_pts, point_size_scaler)
      
      return(invisible(list(dat1=dat1, corMat=corMat, plts=plts)))
  })
}

library(tidyverse)
library(patchwork)
plot_data <- function(dat1, corMat, n_pts, point_size_scaler=1){
  title <- reduce_cor_mat(corMat) %>% {paste(names(.), ., sep = "=", collapse = " ")}
  p1 <- as.data.frame(dat1) %>% 
    ggplot(aes(Y, Z)) +
    # geom_abline(slope = 1, intercept = 0, alpha=.1) +
    geom_point(aes(color=X)) +
    geom_smooth(method="lm", alpha=0.1, size=.2*point_size_scaler, color="black") +
    ggtitle(title) +
    scale_color_viridis_c() +
    theme_minimal() + coord_fixed()
  
  p2 <- as.data.frame(dat1) %>% ggplot() +
    # geom_abline(slope = 1, intercept = 0, alpha=.1) +
    geom_point(aes(X, Z, color="Z"),  size=.4*point_size_scaler, alpha=0.5) +
    geom_point(aes(X, Y, color="Y"),  size=.4*point_size_scaler, alpha=0.5) +
    geom_smooth(aes(X, Z, color="Z"), size=.4*point_size_scaler, alpha=0.5, method="lm") +
    geom_smooth(aes(X, Y, color="Y"), size=.4*point_size_scaler, alpha=0.5, method="lm") +
    ylab("Y/Z") +
    ggtitle(title) +
    theme_minimal() + coord_fixed()
  
  p3 <- as.data.frame(dat1) %>% ggplot() +
    # geom_abline(slope = 1, intercept = 0, alpha=.1) +
    geom_smooth(aes(X, Y), alpha=0.1, size=.2*point_size_scaler, color="black", method="lm") +

    geom_point(aes(X, 
                   color="Y",
                   Y), size=0.4*point_size_scaler, alpha=0.5) +
    geom_segment(aes(x = X, y = Y, 
                     # color=Y>Z
                     xend = X, yend = Z), 
                 alpha=.5, size=.3,
                 arrow = arrow(length = unit(2, "mm"))) +
    # geom_point(aes(X, Z, color="Z"), size=0.7, alpha=0.5) +
    ylab("Y (Z at arrow tip)") +
    ggtitle(title) +
    theme_minimal() + coord_fixed()
  
  print(p1 + p2 + p3)
  plt_list <- list(p1=p1, p2=p2, p3=p3)
  return(invisible(plt_list))
}
```


En el siguiente dataset solo X solo correlaciona con Y.

Hay varios plots:

1. El primero (desde la izquierda) muestra las tres variables, con X en el color.
2. El segundo plotea X vs Y y Z (pero la relación entre Y y Z no aparece). Este grafico es para engañar a la gente.
3. El tercero grafica X vs Y. En cada punto hay una flecha que termina en el valor de Z correspondiente a ese punto.

En el tercer plot se ve como los valores de Z se dispersan para cada punto X-Y; algunos para arriba, otros para abajo. Las flechas terminan en el punto correspondiente XZ que se ve en el plot 2.

El valor de X es fijo para cada punto (porque el X es "compartido") y por eso las flechas solo suben o bajan.

```{r}
res <- gen_data(XY = 0.95, XZ = 0.03, YZ = 0.0)
```

Ahora empieza el juego.

¿Podríamos dejar cor(YZ)=0, pero hacer que Z también correlacione positivamente con X?

En el gráfico anterior, jugar a intercambiar los colores de X entre puntos con un mismo valor de Y. Ese tipo de cambio no modifica la correlación de Y con X, ni la de Y con Z, pero _si_ la de Z con X.

Agrego ahora un poco (0.20) de correlación XZ:

```{r}
set.seed(1)
gen_data(XY = 0.95, XZ = 0.03 + 0.20, YZ = 0.0)
```

En el primer gráfico es sutil, pero se puede ver que X es, en promedio, mayor a mayores valores de Z. Esto queda claro en la línea de tendencia del segundo.

Si intentamos sumarle un poco más, obtenemos un error. No se puede generar datos
multinormales con esa matriz de correlaciones... ¿por qué?

Mirando el gráfico anterior, creo que es porque hay un límite en cuánto se puede maximizar
la correlación de Z con X sin tocar lo demás (jugando al jueguito de intercambiar colores).

```{r}
set.seed(1)
gen_data(XY = 0.95, 
         XZ = 0.03 + 0.30,
         YZ = 0.0)
```

Por ejemplo, si reducimos cor(XY), entonces si podemos aumentar cor(XZ) dejando cor(YZ)=0.

```{r}
set.seed(1)
gen_data(XY = 0.95 - 0.10, XZ = 0.03 + 0.30, YZ = 0.0)

set.seed(1)
gen_data(XY = 0.95 - 0.25, XZ = 0.03 + 0.68, YZ = 0.0)
```

El último gráfico está al limite. Si intento subir XZ da error, a menos que baje XY aún más.

¿Qué lo impide?

Una forma de lograrlo es abandonar la independencia entre Y y Z. En el siguiente gráfico paso cor(YZ) de `0` a `0.8`.

```{r}
gen_data(XY = 0.60, XZ = 0.61, YZ = 0.8)
```

Fíjense cómo el segundo gráfico quedó exactamente igual que antes. El cambio está oculto, y se revela  dibujando las flechas en el gráfico 3: ahora las flechas son más pequeñas.

Hasta ahora todo bien.

Lo preocupante es lo que se observa en el siguiente caso: para las mismas correlaciones _positivas_ entre XY y XZ, es posible tener una correlación _negativa_ entre YZ.

¿Cuán negativa? Bueno no tanto, pero puede serlo!

```{r}
set.seed(1)
gen_data(XY = 0.60,  XZ = 0.61, YZ = -0.26)
```

### Conclusion

Aunque no entiendo del todo este comportamiento, el hecho de que sea posible significa que no es una locura lo que observaba en la distribucion de correlaciones de las simulaciones de Goldbeter.

Ahora no me alarma (aunque si me sigue sorprendiendo) que dos correlaciones sean siempre
positivas y una tercera pueda ser negativa.

```{r}
plot_corr_distributions
```


<!-- ## Save results -->

<!-- ```{r} -->
<!-- ``` -->

