---
title: "Inferencia de redes dinámicas: El Doctorado de Alguien"
author: "Gente"
output:
  html_document:
    df_print: paged
    toc: yes
    toc_float:
      collapsed: false
    toc_depth: 4
    number_sections: false
    smooth_scroll: false
    code_folding: hide
    code_download: true
  pdf_document:
    latex_engine: xelatex
    toc: true
    toc_depth: 4
    number_sections: true
editor_options:
  chunk_output_type: inline
date: "`r format(Sys.time(), '%d %B, %Y')`"
urlcolor: blue
knit: (function(inputFile, encoding) {
  rmarkdown::render(inputFile, encoding = encoding, output_dir = "output/renders") })
---

```{r setup, message=F}
knitr::opts_chunk$set(message = F)
knitr::opts_knit$set(root.dir = here::here())

library(tidyverse)
devtools::load_all()  # Cargar este proyecto como paquete.
# library("docstring")  # Para que el "?" genere un help de las funciones en "R/" sin que sea un paquete.
```

## About

Agregar ruido de adquisición a los datos.

## Load data

Load data from Rk4 with or without added noise during integration (see caveats in the
previous notebook).

```{r}
results.df.full <- read.csv(file=gzfile("data/results.full.df.csv.gz"))

results.df <- read.csv(file=gzfile("data/results.df.csv.gz"))

results.df.noise025 <- read.csv(file=gzfile("data/results.df.noise025.csv.gz"))
```

Uncomment the following line to use the "noisy" integration instead:

```{r}
# results.df <- results.df.noise025
```

## Add acquisition noise

Caveats:

- The uniform acquisition noise does not map exactly to acquisition noise from a 
microscope. Noise is usually autocorrelated, due to the fact that drifts in focus (and 
similar stuff) persist in time, until corrected.
- Focus drift errors/noise affect all measurements similarly and simultáneously, which 
will introduce spurious correlations between variables. It would be best to have a control 
variable that is only subject to this kind of deviation, which can then be used to filter
out the effect from the variables of interest.
  - Far-red constitutively-expressed fluorophores might be useful in this case.

```{r}
set.seed(42)

noisy_data <- results.df %>% 
  # Ruido en el tiempo de adquisición
  mutate(tiempo = tiempo + runif(n = n(), -0.05, 0.05)) %>% 
  # Ruido en la variable C
  mutate(C = C + runif(n = n(), -0.05, 0.05)) %>% 
  # Ruido en la variable M
  mutate(M = M + runif(n = n(), -0.05, 0.05)) %>% 
  # Ruido en la variable X
  mutate(X = X + runif(n = n(), -0.05, 0.05))
```

Plot samples:

```{r}
plot_its <- sample(x = 1:n_its, size = 5)

results.df %>% filter(iter %in% plot_its) %>% 
  ggplot(aes(C, M, color=X, group=iter)) + 
  geom_path() + 
  scale_color_viridis_c() + 
  ggtitle("Muestra de algunas simulaciones") +
  xlim(0, 1.02) + ylim(0, 1.02)

results.df %>% filter(iter %in% plot_its) %>% 
  ggplot() + 
  geom_path(aes(tiempo, C, color="C")) + 
  geom_path(aes(tiempo, M, color="M")) + 
  geom_path(aes(tiempo, X, color="X")) + 
  facet_grid(~iter) + 
  ggtitle("Muestra de algunas simulaciones") +
  ylim(0, 1.02)

noisy_data %>% filter(iter %in% plot_its) %>% 
  ggplot(aes(C, M, color=X, group=iter)) + 
  geom_path() + 
  scale_color_viridis_c() + 
  ggtitle("Muestra de algunas simulaciones") +
  xlim(0, 1.02) + ylim(0, 1.02)

noisy_data %>% filter(iter %in% plot_its) %>% 
  ggplot() + 
  geom_path(aes(tiempo, C, color="C")) + 
  geom_path(aes(tiempo, M, color="M")) + 
  geom_path(aes(tiempo, X, color="X")) + 
  facet_grid(~iter) + 
  ggtitle("Muestra de algunas simulaciones") +
  ylim(0, 1.02)
```

## Save results

```{r}
# noisy_data %>%
#   write.csv(file=gzfile("data/results.df.acq_noise.csv.gz"))
# 
# noisy_data %>%
#   write.csv(file=gzfile("data/results.df.noise025.acq_noise.csv.gz"))
```

