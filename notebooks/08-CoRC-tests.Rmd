---
title: "Inferencia de redes dinámicas: El Doctorado de Alguien"
author: "Nicolás A. Méndez"
output:
  html_document:
    df_print: paged
    toc: yes
    toc_float:
      collapsed: false
    toc_depth: 4
    number_sections: false
    smooth_scroll: false
    code_folding: hide
    code_download: true
  pdf_document:
    latex_engine: xelatex
    toc: true
    toc_depth: 4
    number_sections: true
editor_options:
  chunk_output_type: inline
date: "`r format(Sys.time(), '%d %B, %Y')`"
urlcolor: blue
knit: (function(inputFile, encoding) {
  rmarkdown::render(inputFile, encoding = encoding, output_dir = "output/renders") })
---

## About

Learning how to use the COPASI R Connector package for this project.

Possible tasks:

- [ ] 
- [ ] 
- [ ] 

```{r setup}
library(tidyverse)

if(!requireNamespace("CoRC", quietly = T)) remotes::install_github("jpahle/CoRC")

library(CoRC)
```

## Load model

```{r}
model <- loadSBML("https://ftp.ebi.ac.uk/pub/databases/biomodels/repository/aaa/MODEL6614271263/2/BIOMD0000000003_url.xml")

model
```

## Inspect species

```{r}
species <- getSpecies()
str(species)

species_names <- split(species$key, species$key)
```

## Time course

### Add event

```{r}
trigger_expression = paste0(
  # "{Initial Time} > 5"
  "{Time} > 5"
)

assignment_target <- paste0(
  # "{Cyclin{cell}}"
  "Cyclin"
)

assignment_expression = paste0(
  # "{[Cyclin{cell}]}"
  "{[Cyclin]} + 0.1"
)

try(CoRC::deleteEvent(key = "pulse start"))
CoRC::newEvent(name = "pulse start", 
               trigger_expression = trigger_expression, 
               assignment_target = assignment_target, 
               assignment_expression = assignment_expression)
```

```{r}
trigger_expression = paste0(
  # "{Initial Time} > 5"
  "{Time} > 6"
)

assignment_target <- paste0(
  # "{Cyclin{cell}}"
  "Cyclin"
)

assignment_expression = paste0(
  # "{[Cyclin{cell}]}"
  "{[Cyclin]} - 0.1"
)

try(CoRC::deleteEvent(key = "pulse end"))
CoRC::newEvent(name = "pulse end", 
               trigger_expression = trigger_expression, 
               assignment_target = assignment_target, 
               assignment_expression = assignment_expression)
```

### Run time-course

```{r}
timecourse <- runTimeCourse(duration = 100, intervals = 10000)
```

### Inspect result

```{r}
timecourse$result
```

```{r}
do.call(what = autoplot.copasi_ts, 
        args = c(list(timecourse), species$key) ) +
  theme_minimal()
```

## Unload model

```{r}
unloadModel(model)
# unloadAllModels()
```

