# Identificacion de redes dinámicas

Roadmap: https://gitlab.com/naikymen-space/science/system-id/-/boards

## Goldbeter 1991

Simulaciones producidas con [notebooks/01-RK4_system_sample.Rmd](notebooks/01-RK4_system_sample.Rmd).

Datos finales guardados en [data/results.df.csv.gz](data/results.df.csv.gz).

## Biomodels

Interesantes quizás: <https://www.ebi.ac.uk/biomodels/search?offset=40&numResults=10&sort=relevance-desc&query=*%3A*+AND+TAXONOMY%3A4932+AND+curationstatus%3A%22Manually+curated%22&domain=biomodels>

-   <https://www.ebi.ac.uk/biomodels/BIOMD0000000237>
    -   A modelling approach to quantify dynamic crosstalk between the pheromone and the starvation pathway in baker's yeast.
-   <https://www.ebi.ac.uk/biomodels/BIOMD0000000496>
    -   Large-scale model construction based on a logical layering of data such
        as reaction fluxes, metabolite concentrations, and kinetic constants.

# To-do

1. Simular datos.
  - Con RK4 simple.
  - Con "ruido" en la simulación.
2. Agregar ruido de adquisición.
  - Por ahora solo uniforme e independiente entre variables.
3. Hacer grafiquitos exploratorios.
4. Análisis preliminar I: correlaciones.
  - Resulta que hace falta tener muy buena correlación entre XY e XZ para poder decir algo de YZ sin medirlos simultáneamente. Esto es porque la correlación "no es muy transitiva": https://stats.stackexchange.com/questions/181376/is-correlation-transitive/181380
  - Esto se va a complicar más, porque esa limitación es para variables generadas por un solo proceso, es decir, se espera que esas correlaciones "no cambien" (en el ejemplo del notebook eso quiere decir que las multinormales que las generan no cambian). Sin embargo, como se ve en los gráficos, las correlaciones cambian según el lugar del espacio de fases donde se samplearon los tramos de simulación.
  - La esperanza es que al medir "de a 3" y cambiando solo uno del set, sea un poco más fácil decir algo sobre Z al medir XYW (habiendo medido XYZ en otros individuos). O sea que haya un poco más de "transitividad" en ese caso, o algo así.
  - Sería bueno consultar a Mariela.
5. Análisis preliminar II: correlaciones cont.
  - Seguir explorando las correlaciones en cachitos de trazas.
  - Empezar a ver el tema de inferir la tercera variable con las otras dos, sobre otro dataset con el tercero faltante. Usar datos limpios y "ruidosos".
  - Link al lab meeting: https://drive.google.com/drive/folders/1Kn4fM4iP3d2TnbJC6KJB_yUCt7rF5G69?usp=share_link

Backlog & Discussion:

- [ ] Probar la idea del paper de "automated": dar más peso a los lugares donde hay 
      bifurcaciones en las dinámicas del espacio de fases.
- [ ] Comprimir/alinear las series con DTW.
  - En este aspecto parece útil el gráfico de "fases". Ahí el tiempo no aparece explicitamente,
    y quizás sea una buena forma de "comprimir el tiempo" para armar perfiles.
- [ ] Probar la idea de "coser" dinámicas.
  - Quizás vale la pena pensar en probabilidades de transición entre dinámicas, como si fuera
    un grafo donde cada nodo es una dinámica medida a tiempo finito, y cada borde la probabilidad
    de coser la dinámica de una célula con la de otra.
- [ ] Probar perturbaciones.
  - En las cantidades absolutas: esto no se mapea a una intervención experimental simple,
    salvo para cambios en el medio de cultivo (agregar estímulos) o microinyecciones quizás.
  - En los parámetros de las ODEs: más representativo de una intervención.
  - En algunos casos serían equivalentes: encender una inducción/degradación es en los parámetros
    solo sería una forma más "suave" de mover variables.
